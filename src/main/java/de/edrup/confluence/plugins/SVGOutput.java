package de.edrup.confluence.plugins;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;


public class SVGOutput implements Macro {
	
	private final PageManager pageMan;
	private final AttachmentManager attachmentMan;
	private final PermissionManager permissionManager;

	public SVGOutput(PageManager pageMan, AttachmentManager attachmentMan, PermissionManager permissionManager) {
		this.pageMan = pageMan;
		this.attachmentMan = attachmentMan;
		this.permissionManager = permissionManager;
	}

	@Override
	public String execute(Map<String, String> parameters, String bodyContent,
			ConversionContext conversionContext) throws MacroExecutionException {
		
		// get the right content entity object
		ContentEntityObject ceo = findMyCEO(parameters, conversionContext);
		if(ceo == null) {
			return "svg-out: Sorry, the specified page was not found!";
		}
		
		// check whether the user has access rights to the page
		ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
		if(!permissionManager.hasPermission(confluenceUser, Permission.VIEW, ceo)) {
			return "svg-out: Sorry, you do not have proper access rights to the page where the image is attached to!";
		}
		
		// get the attachment
		// remark: the parameter is called "name" as this is required by the JS
		Attachment svgAttachment = null;
		if(parameters.containsKey("name")) {
			svgAttachment = attachmentMan.getAttachment(ceo, parameters.get("name"));
			if(svgAttachment == null) {
				return "svg-out: Sorry, the attachment was not found at the specified page!";
			}
		}
		else {
			return "svg-out: Internal error: the name parameter is not set!";
		}
		
		// create a document (DOM) from the attachment data
		Document doc = null;
		try {
			// set up a document builder factory
			// remark: the basic principle factory, builder and then parse comes from various sources in the interner
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			
			// disable some DTD validation features as they will cause an exception when the server is not connected to the internet
			// remark: the feature disabling worked so far; other sources indicate to set a new entitiyResolver in the factory which returns an empty stream
			dbFactory.setValidating(false);
			dbFactory.setFeature("http://xml.org/sax/features/namespaces", false);
			dbFactory.setFeature("http://xml.org/sax/features/validation", false);
			dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
			dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			
			// set up the document builder from the factory
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			
			// uncompressed svg
			if(svgAttachment.getFileExtension().equals("svg")) {
				doc = dBuilder.parse(attachmentMan.getAttachmentData(svgAttachment));
			}
			
			// compressed svgz
			else if (svgAttachment.getFileExtension().equals("svgz")) {
				GZIPInputStream gzi = new GZIPInputStream(attachmentMan.getAttachmentData(svgAttachment));
				doc = dBuilder.parse(gzi);
			}
			
			// neither svg nor svgz
			else {
				return "svg-out: File extension " + svgAttachment.getFileExtension() + " is not svg or svgz!";
			}
		}
		catch(IOException | ParserConfigurationException | SAXException e) {
			return e.toString();
		}
		
		// get the scale and cut parameters
		String pScaleX = parameters.containsKey("scaleX") ? parameters.get("scaleX") : "100%";
		String pScaleY = parameters.containsKey("scaleY") ? parameters.get("scaleY") : "100%";
		String pLeft = parameters.containsKey("left") ? parameters.get("left") : "0%";
		String pTop = parameters.containsKey("top") ? parameters.get("top") : "0%";
		String pRight = parameters.containsKey("right") ? parameters.get("right") : "100%";
		String pBottom = parameters.containsKey("bottom") ? parameters.get("bottom") : "100%";
		
		// change the parameters in the SVG we have to
		if(doc != null) {
			
			// get the top most element which is <svg>
			Element svgE = doc.getDocumentElement();
			
			// apply zoom and cut
			try {
				zoomAndCut(svgE, pScaleX, pScaleY, pLeft, pTop, pRight, pBottom);
			}
			catch(NumberFormatException nfe) {
				return "svg-out: Sorry, a number conversion went wrong!";
			}
		}
		
		// transfer the DOM to a string
		StringWriter svgWriter = null;
		try
		{
			// this comes from the Internet - most people seem to do it like this
			DOMSource domSource = new DOMSource(doc);
			svgWriter = new StringWriter();
			StreamResult result = new StreamResult(svgWriter);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
		}
		catch(TransformerException Te)
		{
			return Te.toString();
		}
		
		// replace the links in the SVG if required
		String svgText = "";
		try{
			svgText = linkReplacer(svgWriter.toString(), bodyContent);
		}
		catch(IndexOutOfBoundsException e)
		{
			return "svg-out: Sorry, something went wrong replacing the links...";
		}
		
		// get the alignment parameter
		String pAlign = parameters.containsKey("align") ? parameters.get("align") : "left";
		
		// add an alignment section in case of center or right
		switch(pAlign) {
			case "left": svgText = "<div align=\"left\">" + svgText + "</div>"; break;
			case "center": svgText = "<div align=\"center\">" + svgText + "</div>"; break;
			case "right": svgText = "<div align=\"right\">" + svgText + "</div>"; break;
			case "right with text float": svgText = "<div style=\"margin-left:10px;margin-bottom:10px;float:right\">" +
					 svgText + "</div>"; break;
			default: break;
		}
		
		// return the string representation of the DOM
		return svgText;
	}

	
	@Override
	public BodyType getBodyType() {
		return BodyType.RICH_TEXT;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
	
	
	// find the right ContentEntityObject
	private ContentEntityObject findMyCEO(Map<String, String> parameters, ConversionContext conversionContext) {
		
		// do some init stuff
		ContentEntityObject ceo = null;
		Page page = null;
		
		// find the right page
		// in case the parameter page is not given we take the current entity 
		if(!parameters.containsKey("page")) {
			ceo = conversionContext.getEntity();
		}
		// in case the parameter page is specified
		else {
			// get the parameter page
			String pageParam = parameters.get("page");
			
			// is the "unofficial" parameter "space" given?
			// remark: the JS only sets it in case the space is different from the current space
			if(parameters.containsKey("space")) {
				// get the page using space and page parameter
				String spaceParam = parameters.get("space");
				page = pageMan.getPage(spaceParam, pageParam);
			}
			// in case "space" is not in the list of parameters
			else {
				// we will find the page in our space
				page = pageMan.getPage(conversionContext.getSpaceKey(), pageParam);
			}
		
			// in case we found a page get the entity of it
			if(page != null) {
				ceo = page.getEntity();
			}
		}
		
		// return the ceo we found
		return ceo;
	}
	
	
	// zoom and cut the SVG as required
	private void zoomAndCut(Element svgE, String pScaleX, String pScaleY, String left, String top, String right, String bottom) {
		
		// get the attributes of the SVG element
		String iViewBox = svgE.getAttribute("viewBox");
		String iHeight = svgE.getAttribute("height");
		String iWidth = svgE.getAttribute("width");
		
		/* bug report #1: some SVG sources define height and width in the style attribute
		in case style contains those values we override the existing values and
		delete height and width from the style attribute*/
		String style = svgE.getAttribute("style");
		
		// is the attribute existing?
		if(style.length() > 0) {
			
			// define the regular expressions we need to extract width and height
			final String widthRegex = "width:.*?(;|\\Z)";
			final String heightRegex = "height:.*?(;|\\Z)";
			final String numberRegex = "[0-9.]{1,}";
			
			// get the width out of style
			String interm = regexExtractor(style, widthRegex);
			String dim = regexExtractor(interm, numberRegex);
			if(dim.length() > 0) {
				iWidth = dim;
			}
			
			// get the height out of the style
			interm = regexExtractor(style, heightRegex);
			dim = regexExtractor(interm, numberRegex);
			if(dim.length() > 0) {
				iHeight = dim;
			}
			
			// remove width and height from the style (if they exist)
			style = style.replaceFirst(widthRegex, "");
			style = style.replaceFirst(heightRegex, "");
			style = style.replaceFirst(" *; *\\Z", "");
		}
		
		// remove any existing overflow definition from style and set overflow:hidden
		// overflow:hidden prevents parts of the SVG being drawn outside the viewBox
		style = style.replaceFirst("overflow:.*?(;|\\Z)", "");
		if(style.length() > 0) {
			style = style.concat("; overflow:hidden");
		}
		else {
			style = "overflow:hidden";
		}
		// set the style attribute
		svgE.setAttribute("style", style);
		
		// in case the width and height are not specified but the viewbox we take width and height from the viewbox
		// Adobe Illustrator does such things
		if((iHeight == "") && (iWidth == "") && (iViewBox.length() > 6)) {
			ArrayList<String> viewBoxItems = new ArrayList<String>(Arrays.asList(iViewBox.split(" ")));
			if(viewBoxItems.size() == 4) {
				iWidth = viewBoxItems.get(2);
				iHeight = viewBoxItems.get(3);
			}
		}
		
		// only set the viewBox in case it was not defined in the image
		if(iViewBox == "") {
			// the viewBox will always be the size of the original image (even when scaled!)
			iViewBox = "0 0 " + convert2Unitless(iWidth) + " " + convert2Unitless(iHeight);
		}
		// scale the size of the image
		iWidth = scaleValue(iWidth, pScaleX, left, right);
		iHeight = scaleValue(iHeight, pScaleY, top, bottom);
		
		// cut the viewBox
		iViewBox = cutViewBox(iViewBox, left, top, right, bottom);
		
		// set the attributes
		// remark: in order to scale in X and Y direction differently we have to set preserveAspectRatio to none
		svgE.setAttribute("width", iWidth);
		svgE.setAttribute("height", iHeight);
		svgE.setAttribute("viewBox", iViewBox);
		svgE.setAttribute("preserveAspectRatio", "none");
	}
	
	
	// cut the viewBox so that we only see a part of the image
	public String cutViewBox(String viewBox, String left, String top, String right, String bottom) {
		
		// split the viewBox
		ArrayList<String> viewBoxItems = new ArrayList<String>(Arrays.asList(viewBox.split(" ")));
		
		// in case the viewBox is appropriate (containing 4 elements)
		if(viewBoxItems.size() == 4) {
			
			// calculate the viewBox items
			Double xmin = Double.parseDouble(viewBoxItems.get(0));
			Double ymin = Double.parseDouble(viewBoxItems.get(1));
			Double width = Double.parseDouble(viewBoxItems.get(2));
			Double height = Double.parseDouble(viewBoxItems.get(3));
			
			// calculate the cut viewBox items
			Double xminC = xmin + width * Double.parseDouble(getNumber(left)) / 100;
			Double yminC = ymin + height * Double.parseDouble(getNumber(top)) / 100;
			Double widthC = width * Math.abs(Double.parseDouble(getNumber(right)) - Double.parseDouble(getNumber(left))) / 100;
			Double heightC = height * Math.abs(Double.parseDouble(getNumber(bottom)) - Double.parseDouble(getNumber(top))) / 100;
			
			// and put them together to a string
			viewBox = xminC.toString() + " " + yminC.toString() + " " + widthC.toString() + " " + heightC.toString();
		}
		
		// return the cut viewBox
		return viewBox;
	}
	
	
	// replace the links in the SVG
	private String linkReplacer(String svgText, String bodyContent) throws IndexOutOfBoundsException {
		
		// do some init stuff
		Integer pos_start = 0;
		Integer pos_end = 0;
		
		// we loop as long as we find an href objects in the body content
		while((pos_start = bodyContent.indexOf("<a href=\"", pos_start)) > -1) {
			
			// in case we found one, look for the end
			pos_end = bodyContent.indexOf("\">", pos_start);
			
			// cut out the link
			String new_href = bodyContent.substring(pos_start + 9, pos_end);
			
			// search for the text which is shown as link (this is our search for)
			Integer pos_start_text = pos_end + 2;
			Integer pos_end_text = bodyContent.indexOf("</a>", pos_start_text);
			String old_href = bodyContent.substring(pos_start_text, pos_end_text);
			
			// replace the old link by the new link
			// remark: I put href=" in front so that we do not replace anything else but links
			svgText = svgText.replace("href=\"" + old_href, "href=\"" + new_href);
			
			// the new search position will be the last end position
			pos_start = pos_end;
		}
		
		// return the SVG with replaced links
		return svgText;
	}
	
	
	// get the number part of a string consisting of number and unit
	private String getNumber(String value) {
		return regexExtractor(value, "[-0-9.]{1,}");
	}
	
	
	// get the unit part of a string consisting of number and unit
	private String getUnit(String value) {
		return regexExtractor(value, "[^-0-9.]{1,}");
	}
	
	
	// scale a value consisting of number and unit by a percent value and the cut edges
	private String scaleValue(String value, String scale, String edge1, String edge2) throws NumberFormatException {
		Double d = Double.parseDouble(getNumber(value))
				* Double.parseDouble(getNumber(scale))
				* Math.abs(Double.parseDouble(getNumber(edge2))
						- Double.parseDouble(getNumber(edge1))) / (100 * 100);
		return d.toString() + getUnit(value);
	}
	
	
	// scale a value consisting of number and unit to a number without any unit following the SVG standard
	public String convert2Unitless(String value) throws NumberFormatException {
		String number = getNumber(value);
		String unit = getUnit(value);
		Double number_double = Double.parseDouble(number);
		switch(unit) {
			case "in": number_double *= 90.0; break;
			case "px": break;
			case "mm": number_double *= 3.54; break;
			case "cm": number_double *= 35.4; break;
			case "pc": number_double *= 15.0; break;
			case "pt": number_double *= 1.25; break;
			default: break;
		}
		return number_double.toString();
	}
	
	
	// extract a substring based on the regex given
	public String regexExtractor(String s, String regex) {
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(s);
		if(m.find()) {
			return m.group(0);
		}
		else {
			return "";
		}
	}
}
